package com.orcbank.main;

import com.orcbank.controller.InitController;
import com.orcbank.controller.TestController;

import java.io.*;

import static com.orcbank.utils.Constants.*;

public class Main {
    public static void main(String[] args) throws IOException {

        System.out.println(TestController.runFileTestAccount(TEST_FILE_LOCATION));

        System.out.println(InitController.runFileAccountBank(FILE_LOCATION));

    }
}
