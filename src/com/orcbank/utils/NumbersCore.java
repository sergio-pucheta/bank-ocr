package com.orcbank.utils;

import java.util.Arrays;
import java.util.stream.IntStream;

import static com.orcbank.utils.Constants.*;

public class NumbersCore {

    public  String getStringNumberAccount(String[] accountBank){

        StringBuilder numbersAccount= new StringBuilder();
        IntStream.range(0, 9).forEach(i -> {
            String[] numberText = new String[numbersDimension];
            int startNumber = i * numbersDimension;
            int endNumber = startNumber + numbersDimension;
            numberText[0] = accountBank[0].substring(startNumber, endNumber);
            numberText[1] = accountBank[1].substring(startNumber, endNumber);
            numberText[2] = accountBank[2].substring(startNumber, endNumber);
            numbersAccount.append(convertToNumber(numberText, 0));
        });
        return numbersAccount.toString();
    }


    private  String convertToNumber(String[] numberText,int number){

        if (number>9) return INTER_SIGN;

        if(!Arrays.equals(OLD_NUMBER_LIST.get(number), numberText)){
            return convertToNumber(numberText,number+1);
        } else {
            return String.valueOf(number);
        }
    }
}

