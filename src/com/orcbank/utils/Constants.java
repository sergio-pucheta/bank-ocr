package com.orcbank.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Constants {

    public static int rowAccount = 4;
    public static int numbersDimension=3;

    public static String[] CERO = {" _ ", "| |", "|_|"};
    public static String[] UNO = { "   ", "  |", "  |" };
    public static String[] DOS = { " _ ", " _|", "|_ " };
    public static String[] TRES = { " _ ", " _|", " _|" };
    public static String[] CUATRO = { "   ", "|_|", "  |" };
    public static String[] CINCO = { " _ ", "|_ ", " _|" };
    public static String[] SEIS = { " _ ", "|_ ", "|_|" };
    public static String[] SIETE = { " _ ", "  |", "  |" };
    public static String[] OCHO = { " _ ", "|_|", "|_|" };
    public static String[] NUEVE = { " _ ", "|_|", " _|" };

    public static final List<String[]> OLD_NUMBER_LIST = Collections.unmodifiableList(
            Arrays.asList(CERO, UNO,DOS,TRES,CUATRO,CINCO,SEIS,SIETE,OCHO,NUEVE));

    public static String FILE_LOCATION= "src/example.txt";
    public static String TEST_FILE_LOCATION= "src/exampleTest.txt";

    public static String MESSAGE_TEST= "successful test";
    public static String MESSAGE_INIT= "has been successfully generated";

    public static String INTER_SIGN= "?";
    public static String TEXT_ILL= "ILL";
    public static String TEXT_OK= "OK";
    public static String TEXT_ERR= "ERR";

    public static String FORMAT_DATE= "yyyy-MM-dd 'at' HH:mm:ss z";
    public static String REPORT_BANK_ACCOUNTS= "report_bank_accounts";
    public static String TXT_EXTENSION= ".txt";

    public static String ARRAYS_ACCOUNTS_EXPECT= "arrays accountsExpect";

}
