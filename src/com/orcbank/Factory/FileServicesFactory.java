package com.orcbank.Factory;


import com.orcbank.service.FileServicesImp;
import com.orcbank.service.IFileServices;

public class FileServicesFactory {
    public static IFileServices infoFileDocuments() {
        return new FileServicesImp();
    }
}
