package com.orcbank.Factory;

import com.orcbank.service.AccountServicesImp;
import com.orcbank.service.IAccountServices;

public class AccountServicesFactory {
    public static IAccountServices getAccountFactory() {
        return new AccountServicesImp();
    }
}
