package com.orcbank.controller;


import com.orcbank.Factory.FileServicesFactory;
import com.orcbank.service.IFileServices;
import com.orcbank.test.AccountsTest;

import java.io.IOException;
import java.util.List;


import static com.orcbank.utils.Constants.*;

public class TestController {

    public static String runFileTestAccount(String testFileLocation) throws IOException {

        IFileServices iFileServices = FileServicesFactory.infoFileDocuments();
        //----test functions---------------------
        List<String> exportToTxtTest = iFileServices.fileAccountsDocuments(testFileLocation);
        AccountsTest.testAddition(exportToTxtTest);
        return MESSAGE_TEST;
    }
}
