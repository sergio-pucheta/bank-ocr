package com.orcbank.controller;


import com.orcbank.Factory.FileServicesFactory;

import com.orcbank.service.IFileServices;


import java.io.IOException;
import java.util.List;

import static com.orcbank.utils.Constants.*;

public class InitController {

    public static String runFileAccountBank(String fileLocation) throws IOException {

        IFileServices iFileServices = FileServicesFactory.infoFileDocuments();
        //----init functions---------------------
        List<String> exportToTxt = iFileServices.fileAccountsDocuments(fileLocation);

        iFileServices.generateReport(exportToTxt);
        return MESSAGE_INIT;
    }
}
