package com.orcbank.test;

import junit.framework.TestCase;

import java.util.Arrays;
import java.util.List;

import static com.orcbank.utils.Constants.ARRAYS_ACCOUNTS_EXPECT;

public class AccountsTest extends TestCase {

    public static void testAddition(List<String> currentAccounts) {

        List<String> accountsExpect =  Arrays.asList("000000000 OK","111111111 ERR","222222222 ERR","333333333 ERR","444444444 ERR","555555555 ERR", "666666666 ERR", "777777777 ERR", "888888888 ERR", "999999999 ERR", "123456789 OK", "000000051 OK", "49006771? ILL", "123??678? ILL");
        assertEquals(ARRAYS_ACCOUNTS_EXPECT, accountsExpect, currentAccounts);
    }
}
