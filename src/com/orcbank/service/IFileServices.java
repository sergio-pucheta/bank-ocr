package com.orcbank.service;

import java.io.IOException;
import java.util.List;

public interface IFileServices {

     List<String> fileAccountsDocuments(String fileLocation) throws IOException;
     void generateReport(List<String> exportToTxt) throws IOException;

}
