package com.orcbank.service;

import java.util.List;

public interface IAccountServices {

    List<String> getAccountsBankByDataFile(String[] dataFileDocument);
}
