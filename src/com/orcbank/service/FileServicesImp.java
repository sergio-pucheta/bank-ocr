package com.orcbank.service;

import com.orcbank.Factory.AccountServicesFactory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.orcbank.utils.Constants.*;

public class FileServicesImp implements IFileServices{

    IAccountServices iAccountServices =  AccountServicesFactory.getAccountFactory();

    public  List<String> fileAccountsDocuments(String fileLocation) throws IOException {
        String path = new File(fileLocation).getAbsolutePath();
        System.out.println(path);
        File f = new File(path);
        int fileSize = (int) f.length();
        String[] fileAccounts  = Files.readAllLines(Paths.get(path)).toArray(new String[fileSize]);

        return iAccountServices.getAccountsBankByDataFile(fileAccounts);
    }
    public  void generateReport(List<String> exportToTxt) throws IOException {

        SimpleDateFormat formatter= new SimpleDateFormat(FORMAT_DATE);
        Date date = new Date(System.currentTimeMillis());
        System.out.println(formatter.format(date));
        Files.write(Paths.get(REPORT_BANK_ACCOUNTS.concat(formatter.format(date)).concat(TXT_EXTENSION)), exportToTxt,
                StandardCharsets.UTF_8);
    }
}
