package com.orcbank.service;

import com.orcbank.utils.Constants;
import com.orcbank.utils.NumbersCore;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.orcbank.utils.Constants.*;

public class AccountServicesImp implements IAccountServices{


    public List<String> getAccountsBankByDataFile(String[] dataFileDocument){
        NumbersCore numbersCore = new NumbersCore();
        List<String> accountsBank = Arrays.asList(new String[dataFileDocument.length / rowAccount]);
        int flag = 0;
        for (int i = 0; i < dataFileDocument.length; i += Constants.rowAccount)
        {
            if(dataFileDocument[i]!=null){
                String[] accountBankLine = new String[Constants.numbersDimension];

                accountBankLine[0] = dataFileDocument[i];
                accountBankLine[1] = dataFileDocument[i+1];
                accountBankLine[2] = dataFileDocument[i+2];
                accountsBank.set(flag, numbersCore.getStringNumberAccount(accountBankLine));
                flag++;
            }
        }
        return validateAccounts(accountsBank);
    }
    private String validateAccounts(String accountBank){
        if(accountBank.contains(INTER_SIGN)){
            return TEXT_ILL;
        }
        return mod11Validator(accountBank);
    }

    private String mod11Validator(String accountBank) {

        char[] number = accountBank.toCharArray();
        int sum = 0;
        int flagPosition = accountBank.length();
        for (int i = 0; i < accountBank.length(); i++) {
            int intNumber = Character.getNumericValue(number[i]);
            sum += intNumber * flagPosition;
            flagPosition--;
        }
        float mod11Result;

        mod11Result = sum % 11;
        if (mod11Result == 0){
            return TEXT_OK;
        }
        return TEXT_ERR;
    }
    private List<String> validateAccounts(List<String> accountsBank){
        AccountServicesImp accountServicesImp = new AccountServicesImp();
        return accountsBank.stream().filter(Objects::nonNull).map(s -> s + " " + accountServicesImp.validateAccounts(s)).collect(Collectors.toList());
    }
}
